package oak.forge.commons.bus.guava;

import com.google.common.annotations.*;
import com.google.common.eventbus.*;
import oak.forge.commons.bus.MessageBus;
import oak.forge.commons.bus.MessageHandler;
import oak.forge.commons.data.message.Event;
import oak.forge.commons.data.message.Message;


/**
 * Implementation of {@link MessageBus} based on Guava {@link EventBus} class.
 * Defines single subscriber for all messages.
 * <p>
 * This implementation supresses unstable API usage, since Guava Bus is still in their strange {@link Beta} phase.
 */
@SuppressWarnings("UnstableApiUsage")
public class GuavaMessageBus extends MessageBus {

    private final EventBus eventBus;

    public GuavaMessageBus(EventBus eventBus) {

        this.eventBus = eventBus;
        eventBus.register(this);
    }

    @Override
    public <M extends Message> void post(M message) {
        eventBus.post(message);
    }

    @Override
    public <E extends Event> void subscribe(Class<E> aClass, MessageHandler<E> messageHandler) {

    }

    /**
     * has to be public for subscription mechanism to work!
     */
    @Subscribe
    public <M extends Message> void handleGuavaEvent(M message) {
        handle(message);
    }

}
